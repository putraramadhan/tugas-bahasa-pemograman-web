<?php
 include('koneksi.php'); 

?>
<!DOCTYPE html>
<html>
 <head>
 <title>DATA MAHASISWA UMMY 2022</title>
 <style type="text/css">
 * {
 font-family: "Trebuchet MS";
 }
 h1 {
 text-transform: uppercase;
 color: salmon;
 }
 table {
 border: solid 1px #DDEEEE;
 border-collapse: collapse;
 border-spacing: 0;
 width: 70%;
 margin: 10px auto 10px auto;
 }
 table thead th {
 background-color: #DDEFEF;
 border: solid 1px #DDEEEE;
 color: #336B6B;
 padding: 10px;
 text-align: left;
 text-shadow: 1px 1px 1px #fff;
 text-decoration: none;
 }
 table tbody td {
 border: solid 1px #DDEEEE;
 color: #333;
 padding: 10px;
 text-shadow: 1px 1px 1px #fff;
 }
 a {
 background-color: salmon;
 color: #fff;
 padding: 10px;
 text-decoration: none;
 font-size: 12px;
 }
 </style>
 </head>
 <body>
 <center><h1>Data Mahasiswa</h1><center>
 <center><a href="tambah_data.php">+ &nbsp; Tambah Data</a><center>
 <br/>
 <table>
 <thead>
 <tr>
 <th>No</th>
 <th>Nim</th>
 <th>Nama Mahasiswa</th>
 <th>Jenis Kelamin</th>
 <th>Jurusan</th>
 <th>Alamat</th>
 <th>Action</th>
 </tr>
 </thead>
 <tbody>
 <?php

 $query = "SELECT * FROM tb_mahasiswa ORDER BY nim ASC";
 $result = mysqli_query($koneksi, $query );
 
 if(!$result){
    die ("Query Error: ".mysqli_errno($koneksi)." - ".mysqli_error($koneksi));
    }
    
   
    $no = 1; 
    while($row = mysqli_fetch_assoc($result))
    {
    ?>
    <tr>
    <td><?php echo $no; ?></td>
    <td><?php echo $row['nim']; ?></td>
    <td><?php echo $row['nama_mahasiswa'];?></td>
    <td><?php echo $row['jenis_kelamin']; ?></td>
    <td><?php echo $row['jurusan']; ?></td>
    <td><?php echo $row['alamat']; ?></td>
    
    <td>
    <a href="edit_data.php?nim=<?php echo $row['nim']; ?>">Edit</a> |
    <a href="proses_hapus.php?nim=<?php echo $row['nim']; ?>" onclick="return
    confirm('Anda yakin akan menghapus data ini?')">Hapus</a>
    </td>
    </tr>
   
    
    <?php
    $no++; 
    }
    ?>
    </tbody>
   </table>
   </body>
   </html>
    
   
<body>
<center><h1>data mata kuliah</h1></center>
<center><a href="tambah_matkul.php">+ &nbsp; Tambah data</a></center>
</br>
<table>
</thead>
<tr>
<th>No</th>
 <th>kode Mata Kuliah</th>
 <th>Nama Mata Kuliah</th>
 <th>Jumlah Sks</th>
 <th>Dosen</th>
 <th>Action</th>
</tr>
</thead>
<tbody>
<?php

$query = "SELECT * FROM tb_matkul ORDER BY kode_matkul ASC";
$result = mysqli_query($koneksi, $query);

if(!$result){
   die ("Query Error: ".mysqli_errno($koneksi)." - ".mysqli_error($koneksi));
   }
   
  
   $no = 1; 
   while($row = mysqli_fetch_assoc($result))
   {
   ?>
   <tr>
   <td><?php echo $no; ?></td>
   <td><?php echo $row['kode_matkul']; ?></td>
   <td><?php echo $row['nama_matkul'];?></td>
   <td><?php echo $row['sks']; ?></td>
   <td><?php echo $row['dosen']; ?></td>
   
   <td>
   <a href="edit_matkul.php?kode_matkul=<?php echo $row['kode_matkul']; ?>">Edit</a> |
   <a href="proses_hapus_matkul.php?kode_matkul=<?php echo $row['kode_matkul']; ?>" onclick="return
   confirm('Anda yakin akan menghapus data ini?')">Hapus</a>
   </td>
   </tr>

   <?php
   $no++;
   }
   ?>
   </tbody>
</table>
</body>
</html>